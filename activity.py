

year = int(input("Please input a year \n"))

if year <= 0 and year == 0:
	print(f"0 and negative values are not allowed")
# elif year == str(year):
# 	print(f"String is not allowed")
elif year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")


row = int(input("Enter number of rows \n"))
col = int(input("Enter number of columns \n"))

for each_row in range(row):
	for each_col in range(col):
		print("*", end="")
	print("")